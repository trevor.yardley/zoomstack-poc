# Zoomstack PoC

Ordinance Survey publish their [Zoomstack](https://www.ordnancesurvey.co.uk/products/os-open-zoomstack) product as a GeoPackage (in British National Grid (BNG) - ESPG:27700) and pre-generated Vector Tiles (in Web Mercator (EPSG: 3857))

They don't publish Vector Tiles for BNG at this time. 

This PoC looks at generating a set of Vector Tiles in BNG from the GeoPackage.

## Status

Experimenting: Currently generates tile for a hardcoded square for testing and refinment of the tile generation.

## Capability

* Filter a Geopackage by geospatial area and layer
* Generate a config for the map tile layers (layers-conf.json) in accordance to Ordinance Survey's Zoomstack documentation
* Use GDAL to generate map tiles from the above


## Design

__Overview__

![overview](docs/overview.drawio.png)


| Component               | Remit                                                                                                                                                                                                                                                      | Notes                 |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------|
| Zoomstack Geopackage    | <ul><li>Zoomstack data from OS</li></ul>                                                                                                                                                                                                                   | In BNG                |
| layers.csv              | <ul><li>Defines which layers, and types of feature are present and which zoom level we want them to appear</li></ul>                                                                                                                                       |                       |
| filter-geopackage.sh    | <ul><li>Generates a subset of the original geopackage</li><li>Splits layers by type if we need some types of feature from the layer to be shown at different zoom levels</li></ul>                                                                         | Uses the GDAL library |
| generate-layers-conf.sh | <ul><li>Generate config the tile generator can understand specifying which layers should be included in which zoom level tiles</li><li>In doing this adjusts layer names in the vector tiles where needed to match with OS standard style sheets</li></ul> |                       |
| generate-tiles.sh       | <ul><li>Generates map tiles from the filtered Geopackage and generated layers config</li></ul>                                                                                                                                                             |                       |



### Selecting a subset of the geopackage to generate from 

The script `filter-geopackage.sh` does this for all layers.

Generating from the full zoomstack geopackage in one go causes problems with the fedelity of the tiles generated. But it has been succesful to select a subset of the package (by area) and generate tiles from that. Using an arbitrary rectangle in Bristol and the roads layer as an example to show how to pass an sql filter to ogr2ogr:

```
podman run --rm -it \
    -v ${PWD}:/var/work \
    -v ${DataFolder}:/var/maps  \
    docker.io/osgeo/gdal:ubuntu-full-3.6.3 ogr2ogr \
    -f gpkg \
    /var/work/data/OS_Open_Zoomstack_Test.gpkg \
    /var/maps/OS_Open_Zoomstack.gpkg \
	-nln roads \
    -sql "select * from roads_national where ST_EnvIntersects(roads_national.geom, 361009, 171796, 361409, 172196)"
```

### Tile Generate with ogr2ogr

The script `generate-tiles.sh` does this with the generated `layers-config.json`

[GDAL's](https://gdal.org/) ogr2ogr application has an 'MVT' driver which can read and write Mapbox Vector Tiles: [MVT Driver](https://gdal.org/drivers/vector/mvt.html)

Using the published image you can generate tiles as follows:

```
podman run --rm -it \
    -v ${PWD}:/var/work \
    -v ${MAPS_DIR}:/var/maps  \
    docker.io/osgeo/gdal:ubuntu-full-3.6.3 ogr2ogr \
    -f MVT \
    /var/work/data/oszs_tiles \
    /var/maps/OS_Open_Zoomstack.gpkg \
    -dsco MAXZOOM=9 \
    -dsco FORMAT=DIRECTORY \
    -dsco CONF="{\"land\":{\"target_name\":\"land\",\"minzoom\":0,\"maxzoom\":9},\"boundaries\":{\"target_name\":\"boundaries\",\"minzoom\":5,\"maxzoom\":9},\"national_parks\":{\"target_name\":\"national_parks\",\"minzoom\":5,\"maxzoom\":9},\"roads_national\":{\"target_name\":\"roads\",\"minzoom\":5,\"maxzoom\":9},\"urban_areas\":{\"target_name\":\"urban_areas\",\"minzoom\":5,\"maxzoom\":9}}" \
    -dsco COMPRESS=NO \
    -dsco TILING_SCHEME="EPSG:27700, -238375, 1376256, 1835008"
```

__Podman Breakdown__

* ```-v ${PWD}:/var/work```                     - mount the current directory in the container so we can write output here
* ```-v ${MAPS_DIR}:/var/maps```                - mount the directory where the geopackage can be found in the container
* ```docker.io/osgeo/gdal:ubuntu-full-3.6.3```  - this is a _big_ image but the slimmer versions won't work for MVT generation 

__OGR Command Breakdown__

* ```-f MVT```                           - output Mapbox Vector Tiles
* ```/var/maps/OS_Open_Zoomstack.gpkg``` - the input data
* ```MAXZOOM=9```                        - level to generate tiles up to
* ```FORMAT=DIRECTORY```                 - output the tiles to a directory structure (rather than a package)
* ```CONF=...```                         - layer config - see below
* ```COMPRESS=NO```                      - don't compress the tiles so we can serve them easily off local disk (not a production setting)
* ```TILE_SCHEME```                      - See Tiling Scheme section below

### Tiling Scheme

We need to specify the tile scheme when we generate the Vector Tiles.

It is defined by:

* the coordinate reference system (ESPG:27700)
* the upper left hand coordinates (x,y) of the layer 0 tile, specified in the coordinate reference system (-238375, 1376256)
* The size of the layer 0 tile in meters on the ground (1835008)
* This is included as a parameter in the GDAL command in the format: ```TILING_SCHEME="{crs}, {tile 0 top left x}, {tile 0 top left y}, {tile 0 size}"```

![tile scheme info](docs/info.drawio.png)

These numbers can be found or derived from information in the Ordinance Surveys Vector Tile Service metadata (requires OS Data Hub API key): https://api.os.uk/maps/vector/v1/vts/resources/styles?key={API_KEY}


### Layer Config

The `layer-config.json` file provides a way to specify a mapping between the source ogr layers (so the layers ogr creates when it reads the data in from our intermediate geopackage) and target layers (the layers which will appear in the tiles).

With this you can also specify min and max zoom constraints, so you can filter features by zoom layer.

I've generated this config from `layers.csv` with the `generate-layers-config.sh` script, to make management easier and provides a mechanism to align the vector tile layer names with those used in OS's standard stylesheets (see below).

### Styling

The goal is to use OS's standard stylesheets to style the generated tiles: https://github.com/OrdnanceSurvey/OS-Open-Zoomstack-Stylesheets.git

I used the "outdoor" styles as a tests. 

This requires the stylessheet, the "fonts", "sprites", and "os-open-zoomstack-symbols" folders from the repository above

I had to make two minor changes to the stylesheet to get it to work

1. Change the URLs of the sprites and glyphs to the location I chose to serve them at:
```
    "sprite": "/sprites/sprites",
    "glyphs": "/fonts/{fontstack}/{range}.pbf",
```
2. Add a land style
```
   "layers": [
        ...
        {
            "id": "land",
            "type": "fill",
            "source": "composite",
            "source-layer": "land",
            "layout": {},
            "paint": {"fill-color": "#f5f5f0"}
        },
        ...
   ]
```
These changes are in `web/outdoor-withland.json`

## Use

### Pre-requisites

* Bash
* Podman (or Docker, but you will need to update commands)
* OS Open Zoomstack - `OS_Open_Zoomstack.gpkg`
* Env variable `OSZS_GPKG_PATH` set to the path to the geopackage
* Env variable `OSZS_TILES_DIR` set to the path of a set of map tiles to compare against
* make (to run the makefile jobs, otherwise just use these as a reference for bash commands to run)

### Generating the tiles

* Check that `layers.csv` is configured how you want
* Generate the intermediate geopackage - `make filter-geopackage`
* Generate the layers-config.json - `make generate-layers-config`
* Generate the tiles - `make generate-tiles`

### Serving the Map with OpenLayers

If you keep the paths as I have then you can serve the map page and the tiles from a python http server - `make run-demo`

Then you can access the map at http://localhost:9000

This is based on the example OS provide for serving Vector Tiles from their API: https://labs.os.uk/public/os-data-hub-examples/os-vector-tile-api/vts-27700-basic-map#openlayers

### Extracting mbtiles for comparison

There is commented out code in the demo if you want to see OS's generated tiles next to the generated tiles.

To use this you'll need to extract OS's tiles from the mbtiles package they are supplied in.

To do this I used their container `https://hub.docker.com/r/ordnancesurvey/whalebrew-mbutil#!`

See their repo for more instructions
```
./mb-util --image_format=pbf .mbtiles {mbtiles output directory}
gzip -d -r -S .pbf *
```
