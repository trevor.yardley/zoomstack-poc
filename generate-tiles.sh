#! /bin/sh

conf=$(cat layers-conf.json)

podman run --rm -it \
    -v ${PWD}:/var/work \
    docker.io/osgeo/gdal:ubuntu-full-3.6.3 ogr2ogr \
    -f MVT \
    /var/work/data/tiles \
    /var/work/data/Filtered.gpkg \
    -dsco MINZOOM=9 \
    -dsco MAXZOOM=14 \
    -dsco FORMAT=DIRECTORY \
    -dsco COMPRESS=NO \
    -dsco CONF="$conf" \
    -dsco TILING_SCHEME="EPSG:27700, -238375, 1376256, 1835008"
