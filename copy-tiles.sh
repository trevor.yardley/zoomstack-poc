#! /bin/bash

function coord_to_tile {	
    coord=$1
    zoom=$2
    coordOrigin=$3
    t0_dimension=$4
    
    tzoom_dimension=$(( $t0_dimension / $(( 2 ** $zoom )) ))
    tile=$(( $(($coord - $coordOrigin)) / $tzoom_dimension ))
    echo $tile
}

x1=$1
y1=$2
x2=$3
y2=$4
xorigin=$5
yorigin=$6
t0_dimension=$7
min_zoom=$8
max_zoom=$9

for zoom in `seq $min_zoom $max_zoom`
do
    tx0=$(coord_to_tile $x1 $zoom $xorigin $t0_dimension)
    ty0=$(coord_to_tile $yorigin $zoom $(($y1+1)) $t0_dimension)
    tx1=$(coord_to_tile $x2 $zoom $xorigin $t0_dimension)
    ty1=$(coord_to_tile $yorigin $zoom $(($y2+1)) $t0_dimension)

    echo "copying tiles from $tx0, $ty0 to $tx1, $ty1" >> log.txt
    for tx in `seq $tx0 $tx1`
    do
	for ty in `seq $ty1 $ty0`
	do
	    mkdir -p data/tiles-combined/$zoom/$tx/ && cp data/tiles/$zoom/$tx/$ty.pbf data/tiles-combined/$zoom/$tx/$ty.pbf
	done
    done
done

 
