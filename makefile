filter-geopackage:
	rm -f data/Filtered.gpkg
	./filter-geopackage.sh 361009 171796 366009 176796

show-source-geopackage:
	podman run --rm -it \
		-v ${PWD}:/var/work \
		-v ${OSZS_GPKG_PATH}:/var/maps/OS_Open_Zoomstack.gpkg  \
		docker.io/osgeo/gdal:ubuntu-full-3.6.3 ogrinfo \
		/var/maps/OS_Open_Zoomstack.gpkg \
		-sql "select * from gpkg_contents"

show-filtered-geopackage:
	podman run --rm -it \
		-v ${PWD}:/var/work \
		-v ${OSZS_GPKG_PATH}:/var/maps/OS_Open_Zoomstack.gpkg  \
		docker.io/osgeo/gdal:ubuntu-full-3.6.3 ogrinfo \
		/var/work/data/Filtered.gpkg \
		-sql "select * from gpkg_contents"

generate-layers-config:
	./generate-layers-config.sh

generate-tiles:
	rm -rf data/tiles
	./generate-tiles.sh 361009 171796 366009 176796

export-mbtiles:
	podman run --rm -it \
		-v ${PWD}:/var/work \
		-v ${OSZS_GPKG_PATH}:/var/maps/OS_Open_Zoomstack.gpkg \
		-w /var/maps \
		docker.io/ordnancesurvey/whalebrew-mbutil ./mb-util --image_format=pbf OS_Open_ZoomStack.mbtiles oszs_mbtiles

generate-all:
	./iterate.sh 363737 168448 3584 3 3

run-demo:
	podman run --rm -it \
		-v ${PWD}/web:/var/web \
		-v ${PWD}/data:/var/web/data \
		-v ${OSZS_TILES_DIR}:/var/web/oszs_mbtiles \
		-w /var/web -p 9000:9000 \
		docker.io/python:3 python3 -m http.server 9000
