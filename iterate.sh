#! /bin/sh

startx=$1
starty=$2
blocksize=$3
rows=$4
columns=$5

rm -rf data/tiles-combined
mkdir data/tiles-combined

for column in `seq 0 $columns`
do
    for row in `seq 0 $rows`
    do
	echo "processing block - row: $row, column: $column"
	echo "processing block - row: $row, column: $column" >> log.txt
	
	# calculate x and y
	x1=$(( $startx + $(($column * $blocksize)) ))
	x2=$(( $x1 + $blocksize - 1))
	y1=$(( $starty + $(($row * $blocksize)) ))
	y2=$(( $y1 + $blocksize - 1))
	echo "block coords - x1: $x1, y1: $y1, x2: $x2, y2: $y2"
	echo "block coords - x1: $x1, y1: $y1, x2: $x2, y2: $y2" >> log.txt

	# filter the package to current block
	echo " - filtering the geopackage"
	rm -f data/Filtered.gpkg
	./filter-geopackage.sh $x1 $y1 $x2 $y2
	echo " - package filtered"
	
	# generate the tiles for this block
	echo " - generating tiles"
	rm -rf data/tiles
	./generate-tiles.sh
	echo " - tiles generated"
	
	# fold the tiles into the main set
	echo " - combining tiles"
	./copy-tiles.sh $x1 $y1 $x2 $y2 -238375 1376256 1835008 9 14
	echo " - tiles combined"
    done
done
