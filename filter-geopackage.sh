#! /bin/bash

# $1 - target layer name
# $2 - source layer name
# $3 - type within the layer
# $4 - is this the base layer (omit if not)
# $5 - x1 Top left
# $6 - y1
# $7 - x2 Bottom right
# $8 - y2
function append_layer {	
    if [ -z "$3" ]; then query=""; else query="and type = '$3'"; fi
    if [ -n "$4" ]; then update=""; else update="-update"; fi
    		      
    podman run --rm \
	   -v ${PWD}:/var/work \
	   -v ${OSZS_GPKG_PATH}:/var/maps/OS_Open_Zoomstack.gpkg \
	   docker.io/osgeo/gdal:ubuntu-full-3.6.3 ogr2ogr \
	   -f gpkg \
	   /var/work/data/Filtered.gpkg \
	   /var/maps/OS_Open_Zoomstack.gpkg \
	   $update -nln "$1" \
	   -sql "select * from $2 where ST_EnvIntersects($2.geom, $5, $6, $7, $8) $query"
}

x1="$1" # Top left
y1="$2"
x2="$3" # Bottom right
y2="$4"
first=true

while IFS="," read -r layer type minZoom maxZoom
do
    if [ -z "$type" ]; then
	type=""
	newLayer="$layer";
    else
	cleanType="$(tr [A-Z] [a-z] <<< "${type// /_}")"	
	newLayer="${layer}_${cleanType}";
    fi
    append_layer "$newLayer" "$layer" "$type" "$first" "$x1" "$y1" "$x2" "$y2"
    first=
done < <(tail -n +2 layers.csv)
