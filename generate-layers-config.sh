#! /bin/bash

# $1 - target layer name
# $2 - source layer name
# $3 - min zoom
# $4 - max zoom
# $5 - is this the first layer (omit if not)
function generate_layer {
    targetLayer=$1
    sourceLayer=$2
    minZoom=$3
    maxZoom=$4
    if [ -n "$5" ]; then leadingComma=""; else leadingComma=","; fi

    echo -n "$leadingComma'$sourceLayer': { 'target_name':'$targetLayer', 'minzoom': '$minZoom', 'maxzoom': '$maxZoom' }"
}

echo -n "{">layers-conf.json
first=true
while IFS="," read -r layer type minZoom maxZoom
do
    echo "generating layer: $layer, type:$type, min-zoom:$minZoom, max-zoom:$maxZoom"
    
    if [ -z "$type" ]; then
	type=""
	newLayer="$layer";
    else
	cleanType="$(tr [A-Z] [a-z] <<< "${type// /_}")"	
	newLayer="${layer}_${cleanType}";
    fi

    oldLayer=$layer
    if [ "$layer" = "local_buildings" ]; then oldLayer="buildings"; fi
    if [ "$layer" = "district_buildings" ]; then oldLayer="buildings"; fi    
    if [ "$layer" = "railway_stations" ]; then oldLayer="railwaystations"; fi
    if [ "$layer" = "roads_local" ]; then oldLayer="roads"; fi
    if [ "$layer" = "roads_regional" ]; then oldLayer="roads"; fi
    if [ "$layer" = "roads_national" ]; then oldLayer="roads"; fi
    if [ "$layer" = "greenspace" ]; then oldLayer="greenspaces"; fi
    
    generate_layer "$oldLayer" "$newLayer" "$minZoom" "$maxZoom" "$first" >> layers-conf.json
    first=
done < <(tail -n +2 layers.csv)
echo -n "}">>layers-conf.json
