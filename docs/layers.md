# Layers

{
\"land\":
{\"target_name\":\"land\",\"minzoom\":0,\"maxzoom\":11},
\"boundaries\":
{\"target_name\":
\"boundaries\",\"minzoom\":6,\"maxzoom\":11},
\"national_parks\":
{\"target_name\":\"national_parks\",\"minzoom\":0,\"maxzoom\":8}
,\"roads_national\":
{\"target_name\":\"roads\",\"minzoom\":2,\"maxzoom\":11},
\"urban_areas\":
{\"target_name\":\"urban_areas\",\"minzoom\":0,\"maxzoom\":8},
\"airports\":
{\"target_name\":\"airports\",\"minzoom\":6,\"maxzoom\":11},
\"contours\":
{\"target_name\":\"contours\",\"minzoom\":6,\"maxzoom\":11},
\"district_buildings\":
{\"target_name\":\"district_buildings\",\"minzoom\":8,\"maxzoom\":9},
\"etl\":
{\"target_name\":\"etl\",\"minzoom\":9,\"maxzoom\":11},
\"foreshore\":
{\"target_name\":\"foreshore\",\"minzoom\":6,\"maxzoom\":11},
\"greenspace\":
{\"target_name\":\"greenspace\",\"minzoom\":7,\"maxzoom\":11},
\"local_buildings\":
{\"target_name\":\"local_buildings\",\"minzoom\":10,\"maxzoom\":11},
\"names\":
{\"target_name\":\"names\",\"minzoom\":2,\"maxzoom\":11},
\"roads_regional\":
{\"target_name\":\"roads_regional\",\"minzoom\":6,\"maxzoom\":11},
\"sites\":
{\"target_name\":\"sites\",\"minzoom\":7,\"maxzoom\":11},
\"railway_stations\":
{\"target_name\":\"railway_stations\",\"minzoom\":9,\"maxzoom\":11},
\"rail\":
{\"target_name\":\"rail\",\"minzoom\":7,\"maxzoom\":11},
\"roads_local\":
{\"target_name\":\"roads_local\",\"minzoom\":8,\"maxzoom\":11},
\"surfacewater\":
{\"target_name\":\"surfacewater\",\"minzoom\":4,\"maxzoom\":11},
\"waterlines\":
{\"target_name\":\"waterlines\",\"minzoom\":2,\"maxzoom\":11},
\"woodland\":
{\"target_name\":\"woodland\",\"minzoom\":3,\"maxzoom\":11}}"




airports - 10-14
boundaries - 5-14
buildings - 11-14
contours / normal - 12 - 14
contours / index - 9-14
etl - 12-14
foreshore - 9-14
greenspaces - 10-14
names / country
names / capital
name / national park
names / city
names / town
names / village
names / hamlet
names / small settlements
names / suburban area
names / woodland
names / landform
names / landcover
names / water
names / greenspace
names / sites
national parks - 5- 14
railway stations 12-14
rail / multitrack - 10-14
rail / single track - 10-14
rail / narrow guage - 14
rail / tunnel - 10-14
roads / motorway - 
roads / primary
roads / a road
roads / b road
roads / guided bus way
roads / minor
roads / local
roads / restricted
roads / tunnels
sea - 0-14
sites - 11 - 14
surfacewater - 6 - 14
urban areas - 5 - 10
waterlines / national - 8
waterlines / regional - 9
waterlines / district - 10-11
waterlines / local - 12-14
waterlines / mhw - 9-14
waterlines / mlw - 9-14
woodland - 6






